
var sum = (...theArgs) => theArgs.reduce((previous, current) =>  previous + current);
var sub = (...theArgs) => theArgs.reduce((previous, current) =>  previous - current);
var mul = (...theArgs) => theArgs.reduce((previous, current) =>  previous * current);
var div = (...theArgs) => theArgs.reduce((previous, current) =>  previous / current);

export {sum, sub, mul, div};
export default mul;